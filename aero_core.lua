--[[--------------------------------------------------------
Applies an aerodynamic force to an physics object which
simulates the effect of stabilizing fins.
 - phys: physics object to which to apply the force
 - location: the location on the object at which to apply the force
 - strength: the strength of the force to apply
-------------------------------------------------------------]]
function ApplyAeroForce(phys, location, strength)
	
	if (phys:IsValid()) then
	
		local entity = phys:GetEntity()

		-- Get the direction that the physics object is travelling through the air (or water)
		local motion = phys:GetVelocity():GetNormalized()
		
		-- Get the direction pointing backwards through the object.
			-- This is the theoretical direction air (or water) would be
			-- travelling when deflected off of the simulated stabilizing fins
		-- TODO: this should be refactored to compute the direction as the
			-- normal vector through the physics object's center of mass and the
			-- point specified by the location parameter
		local backwards = entity:GetForward():GetNormalized()	
		backwards:Mul(-1)
		
		-- Get the normalized vector of the force normal to the simulated stabilizing fin(s).
			-- (There is a bug in GetNormalized() preventing it from being chained after a :Cross() call.)
		local deflection = motion:Cross(backwards):Cross(backwards)		
		deflection:Normalize()
		
		-- Compute the magnitude of the deflection force as a function of velocity,
			-- angle of deflection, and the strength parameter
		local multiplier = (phys:GetVelocity():LengthSqr() * .001) * strength * -motion:Dot(deflection)
		
		-- Get the actual vector for the deflection force
		local forceVector = deflection		
		forceVector:Mul(multiplier)
		
		-- Calculate the world coords of the point on the physics object at which to apply the deflection force
		-- TODO: the rotation angle should be calculated off of the backwards vector,
			-- rather that and entity's local angles.
		local point = location
		point:Rotate(entity:GetLocalAngles())
		point:Add(phys:GetPos())	
		
		-- Apply the deflection force.
		phys:ApplyForceOffset(forceVector,point)
	end
end

function GetImpactVelocity(colData, collider)
	local dirEnt = collider:GetVelocity()
	dirEnt:Normalize()	
	local dotEnt = math.abs(dirEnt:Dot(colData.HitNormal))
	
	local dirHit = colData.HitObject:GetVelocity()
	dirHit:Normalize()
	local dotObj = math.abs(dirHit:Dot(colData.HitNormal))
	
	local speedEnt = collider:GetVelocity()
	speedEnt:Mul(dotEnt)
	
	local speedHit = colData.HitObject:GetVelocity()
	speedHit:Mul(dotHit)
	
	speedEnt:Sub(speedHit)
	
	return speedEnt:Length()	
end

function ApplyExplosion(phys)
	if(phys:IsValid()) then
		local point = phys:GetPos()
		local entity = phys:GetEntity()
		
		util.BlastDamage( entity, entity, point, 800, 250 )
		entity:Remove()
		
		local explosion = ents.Create("env_explosion")
		explosion.Magnitude = 1000
		local physExplosion = ents.Create("env_physexplosion")
		physExplosion.Magnitude = 1000
		
		explosion:SetPos(point)
		physExplosion:SetPos(point)
		
		explosion:Fire("explode")
		physExplosion:Fire("explode")
		
		explosion:Remove()
		physExplosion:Remove()
	end
end