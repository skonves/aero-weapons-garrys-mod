AddCSLuaFile()
AddCSLuaFile('aero_core.lua')
include('aero_core.lua')

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.Category = "Aero Weapons"
ENT.Spawnable = true
 
ENT.PrintName		= "Aero Bomb"
ENT.Author			= "Steve Konves"
ENT.Instructions	= "Aerodynamically Correct Bomb"

function ENT:Initialize() 
	if ( SERVER ) then
		self:SetModel( "models/props_phx/mk-82.mdl" )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetHealth(10)
	 
		local phys = self:GetPhysicsObject()
		if (phys:IsValid()) then
			phys:Wake()
			phys:SetMass(2000)
		end
	end
end

if (CLIENT) then
	function ENT:Draw()
		self:DrawModel()
	end
end

function ENT:Think()
	ApplyAeroForce(self:GetPhysicsObject(), Vector(-200,0,0), 3)	
end

function ENT:PhysicsCollide(colData, collider)
	if(not self:IsPlayerHolding() and GetImpactVelocity(colData, collider) > 550) then
		ApplyExplosion(collider)
	end
end

function ENT:OnTakeDamage(damage)
	if self:Health() > 0 and damage:GetInflictor() ~= self then	
		self:SetHealth(self:Health() - damage:GetDamage())	
		if(self:Health() <= 0) then
			ApplyExplosion(self:GetPhysicsObject())
		end		
	end
end